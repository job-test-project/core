import { Injectable, Logger } from '@nestjs/common';
import { Cron } from '@nestjs/schedule';

import { blocksRepository } from '../../../models';
import { BlocksService } from '../../abstract/blocks-service';

@Injectable()
export class SyncNewBlocksService extends BlocksService {
  public readonly logger = new Logger(SyncNewBlocksService.name);
  private isRunning = false;

  private async syncNewBlocks(): Promise<void> {
    const latestBlockTag = await BlocksService.etherDebouncer(() =>
      BlocksService.etherscan.getLastBlockNumber(),
    );
    const latestBlockNumber = parseInt(latestBlockTag, 16);

    this.logger.debug(
      `Latest block number is ${latestBlockNumber} (${latestBlockTag})`,
    );

    const latestSavedBlock = await blocksRepository.findLatestBlock();

    let blockNumber = latestBlockNumber;
    if (latestSavedBlock) {
      blockNumber = Number(latestSavedBlock.id) + 1;

      if (blockNumber > latestBlockNumber) {
        return;
      }
    }

    this.logger.debug(`Syncing next block with number ${blockNumber}`);
    await this.processBlockNumber(blockNumber, true);
  }

  @Cron('*/20 * * * * *')
  async handleCron(): Promise<void> {
    if (this.isRunning) {
      this.logger.warn(`Job is already running, aborting...`);
      return;
    }
    this.isRunning = true;

    this.logger.log('Job started');

    try {
      const startTime = Date.now();

      await this.syncNewBlocks();

      this.logger.log(`Job finished in ${Date.now() - startTime}ms`);
    } catch (e) {
      this.logger.error(`Job failed - ${e.message}`);
    }

    this.isRunning = false;
  }
}
