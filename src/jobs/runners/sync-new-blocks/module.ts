import { Module } from '@nestjs/common';
import { ScheduleModule } from '@nestjs/schedule';

import { SyncNewBlocksService } from './service';

@Module({
  providers: [SyncNewBlocksService],
  imports: [ScheduleModule.forRoot()],
})
export class SyncNewBlocksModule {}
