import '@project/project-models';

import { BlocksRepository } from './repositories/blocks';
import { TxsRepository } from './repositories/txs';

export const blocksRepository = new BlocksRepository();
export const txsRepository = new TxsRepository();
