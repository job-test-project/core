import { Module } from '@nestjs/common';
import { ScheduleModule } from '@nestjs/schedule';

import { SaveOldBlocksService } from './service';

@Module({
  providers: [SaveOldBlocksService],
  imports: [ScheduleModule.forRoot()],
})
export class SaveOldBlocksModule {}
