import axios, { AxiosError, AxiosInstance } from 'axios';

import { etherscanConfig } from '../../configs/etherscan';
import { EtherscanError } from './error';
import { IEtherscanBlock, JRPCError, RequestResponse } from './types';

export class EtherscanClient {
  private api: AxiosInstance;

  constructor() {
    this.api = axios.create({
      baseURL: etherscanConfig.baseURL,
    });
  }

  protected parseError(error: AxiosError<JRPCError>): EtherscanError | Error {
    if (error.response?.data?.error) {
      const data = error.response?.data?.error;
      return new EtherscanError(data.message, data.code);
    }

    return error;
  }

  protected parseResponse<T>(response: RequestResponse<T>): T {
    if ('status' in response) {
      throw new EtherscanError(
        `'${response.result}' - '${response.message}'`,
        Number(response.status),
      );
    }
    if ('error' in response) {
      throw new EtherscanError(response.error.message, response.error.code);
    }
    if (response.id === null) {
      throw new EtherscanError(JSON.stringify(response), null);
    }
    return response.result;
  }

  async getBlockByNumber(params: { tag: string }): Promise<IEtherscanBlock> {
    try {
      const { data } = await this.api.get<RequestResponse<IEtherscanBlock>>(
        '',
        {
          params: {
            module: 'proxy',
            action: 'eth_getBlockByNumber',
            tag: params.tag,
            boolean: true,
          },
        },
      );

      return this.parseResponse(data);
    } catch (e) {
      throw this.parseError(e);
    }
  }

  async getLastBlockNumber(): Promise<string> {
    try {
      const { data } = await this.api.get<RequestResponse<string>>('', {
        params: {
          module: 'proxy',
          action: 'eth_blockNumber',
        },
      });

      return this.parseResponse(data);
    } catch (e) {
      throw this.parseError(e);
    }
  }
}
