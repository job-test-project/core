import { Module } from '@nestjs/common';

import { SaveOldBlocksModule } from './jobs/runners/save-old-blocks/module';
import { SyncNewBlocksModule } from './jobs/runners/sync-new-blocks/module';
import { UpdateFailedBlocksModule } from './jobs/runners/update-failed-blocks/module';

@Module({
  imports: [SaveOldBlocksModule, SyncNewBlocksModule, UpdateFailedBlocksModule],
})
export class AppModule {}
