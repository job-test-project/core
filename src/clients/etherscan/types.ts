interface JRPC {
  jsonrpc: string;
  id: number;
}

export interface JRPCError extends JRPC {
  error: {
    code: number;
    message: string;
  };
}

export interface JRPCResult<T> extends JRPC {
  result: T;
}

export interface ServerError {
  status: string;
  message: string;
  result: string;
}

export type RequestResponse<T> = JRPCError | JRPCResult<T> | ServerError;

export interface IEtherscanTx {
  blockHash: string;
  blockNumber: string;
  from: string;
  gas: string;
  gasPrice: string;
  hash: string;
  input: string;
  nonce: string;
  to: string;
  transactionIndex: string;
  value: string;
  type: string;
  v: string;
  r: string;
  s: string;
}

export interface IEtherscanBlock {
  difficulty: string;
  extraData: string;
  gasLimit: string;
  gasUsed: string;
  hash: string;
  logsBloom: string;
  miner: string;
  mixHash: string;
  nonce: string;
  number: string;
  parentHash: string;
  receiptsRoot: string;
  sha3Uncles: string;
  size: string;
  stateRoot: string;
  timestamp: string;
  totalDifficulty: string;
  transactions: IEtherscanTx[];
  transactionsRoot: string;
  uncles: unknown[];
}
