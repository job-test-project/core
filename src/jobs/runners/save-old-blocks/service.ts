import { Injectable, Logger } from '@nestjs/common';
import { Cron } from '@nestjs/schedule';

import { constants } from '../../../configs/constants';
import { blocksRepository } from '../../../models';
import { BlocksService } from '../../abstract/blocks-service';

@Injectable()
export class SaveOldBlocksService extends BlocksService {
  public readonly logger = new Logger(SaveOldBlocksService.name);
  private isRunning = false;

  private async saveOldBlocks(): Promise<void> {
    this.logger.debug(`Fetching oldest block`);

    const oldestBlock = await blocksRepository.getOldestBlock();

    if (!oldestBlock) {
      this.logger.warn('No blocks in database, aborting...');
      return;
    }

    const blockNumber = Number(oldestBlock.id) - 1;

    if (blockNumber < constants.lowestHeight) {
      this.logger.debug(`All blocks saved up to ${constants.lowestHeight}`);
      return;
    }

    await this.processBlockNumber(blockNumber, true);
  }

  @Cron('*/20 * * * * *')
  async handleCron(): Promise<void> {
    if (this.isRunning) {
      this.logger.warn(`Job is already running, aborting...`);
      return;
    }
    this.isRunning = true;

    this.logger.log('Job started');

    try {
      const startTime = Date.now();

      await this.saveOldBlocks();

      this.logger.log(`Job finished in ${Date.now() - startTime}ms`);
    } catch (e) {
      this.logger.error(`Job failed - ${e.message}`);
    }

    this.isRunning = false;
  }
}
