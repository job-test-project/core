import { BlocksEntity, EBlockStatus } from '@project/project-models';

export class BlocksRepository {
  async getOldestBlock(): Promise<BlocksEntity> {
    return await BlocksEntity.findOne({
      order: [['id', 'ASC']],
      raw: true,
    });
  }

  async findLatestBlock(): Promise<BlocksEntity> {
    return await BlocksEntity.findOne({
      order: [['id', 'DESC']],
      raw: true,
    });
  }

  async updateOne(
    id: string,
    data: Partial<BlocksEntity>,
  ): Promise<BlocksEntity> {
    const [, instances] = await BlocksEntity.update(
      {
        ...data,
        updated_at: new Date(),
      },
      {
        where: {
          id: id,
        },
        returning: true,
      },
    );

    return instances?.[0];
  }

  async findOneById(id: string): Promise<BlocksEntity> {
    return await BlocksEntity.findOne({
      where: {
        id: id,
      },
      raw: true,
    });
  }

  async findLatestFailedBlock(): Promise<BlocksEntity> {
    return await BlocksEntity.findOne({
      where: {
        status: EBlockStatus.ERROR,
      },
      order: [['id', 'DESC']],
      raw: true,
    });
  }

  async saveBlock(params: {
    id: string;
    hash: string;
    status: EBlockStatus;
  }): Promise<BlocksEntity> {
    return await BlocksEntity.create(params);
  }
}
