import { Logger } from '@nestjs/common';
import { BlocksEntity, EBlockStatus, sequelize } from '@project/project-models';

import { EtherscanClient } from '../../clients/etherscan';
import { IEtherscanBlock } from '../../clients/etherscan/types';
import { etherscanConfig } from '../../configs/etherscan';
import { blocksRepository, txsRepository } from '../../models';
import { getDebouncer } from '../../utils/debounce';

export abstract class BlocksService {
  abstract logger: Logger;
  protected static etherscan = new EtherscanClient();
  protected static etherDebouncer = getDebouncer(
    etherscanConfig.timeoutPerRequest,
  );

  protected async saveNewBlock(params: {
    isNew: boolean;
    blockNumber: number;
    block: IEtherscanBlock | null;
  }): Promise<void> {
    let savedBlock: BlocksEntity;
    if (params.isNew) {
      this.logger.debug(`Saving new block with number ${params.blockNumber}`);

      savedBlock = await blocksRepository.saveBlock({
        id: String(params.blockNumber),
        hash: params.block?.hash,
        status: params.block ? EBlockStatus.SUCCESS : EBlockStatus.ERROR,
      });
    } else {
      if (!params.block) {
        this.logger.warn(
          `Nothing to update for block ${params.blockNumber}, aborting...`,
        );
        return;
      }

      this.logger.debug(`Updating block with number ${params.blockNumber}`);
      savedBlock = await blocksRepository.updateOne(
        String(params.blockNumber),
        {
          status: EBlockStatus.SUCCESS,
        },
      );
    }

    this.logger.debug(
      `Successfully ${params.isNew ? 'saved' : 'updated'} block ${
        params.blockNumber
      } with status '${savedBlock.status}'`,
    );

    if (!params.block) {
      return;
    }

    try {
      await sequelize.transaction(async (t) => {
        await Promise.all(
          params.block.transactions.map(async (tx) => {
            await txsRepository.create(
              {
                hash: tx.hash,
                from: tx.from,
                to: tx.to,
                value: tx.value,
                blockId: savedBlock.id,
              },
              { t: t },
            );
          }),
        );
      });

      this.logger.debug(
        `Successfully saved all ${params.block.transactions.length} transactions in block ${params.blockNumber}`,
      );
    } catch (e) {
      this.logger.error(
        `Failed to save transactions for block ${savedBlock.id} - ${e.message}`,
      );

      await blocksRepository.updateOne(savedBlock.id, {
        status: EBlockStatus.ERROR,
      });

      this.logger.warn(
        `Set status for block number ${savedBlock.id} back to '${EBlockStatus.ERROR}'`,
      );
    }
  }

  protected async processBlockNumber(
    blockNumber: number,
    isNew: boolean,
  ): Promise<void> {
    let block: IEtherscanBlock = null;

    try {
      block = await BlocksService.etherDebouncer(() =>
        BlocksService.etherscan.getBlockByNumber({
          tag: blockNumber.toString(16),
        }),
      );
    } catch (e) {
      this.logger.error(`Failed to fetch block ${blockNumber} - ${e.message}`);
    }

    try {
      await this.saveNewBlock({
        isNew: isNew,
        blockNumber: blockNumber,
        block: block,
      });
      // this.logger.debug(`Successfully saved block ${blockNumber}`)
    } catch (e) {
      this.logger.error(`Failed to save block ${blockNumber} - ${e.message}`);
    }
  }
}
