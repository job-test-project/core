import { Module } from '@nestjs/common';
import { ScheduleModule } from '@nestjs/schedule';

import { UpdateFailedBlocksService } from './service';

@Module({
  providers: [UpdateFailedBlocksService],
  imports: [ScheduleModule.forRoot()],
})
export class UpdateFailedBlocksModule {}
