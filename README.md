# Некоторые решения

## Почему core отдельно от api

Пока в NestJS c архитектурой Monorepo нет функции вызова скриптов в package.json дочерних проектов (как у Lerna, Rush и тд), что не позволяет деплоить проекты отдельно.

## Почему важно деплоить отдельно

Для кора мб еще не критично, но с api напрямую взаимодействует end-user и downtime нужно минимизировать.

## Почему несколько джоб?

Разделение обязанностей, тк при выполнение всех трех функций, одна из них может зависнуть и другой функционал перестанет работать.

## Почему на запрос к EtherScan задержка в 20 сек?

В доке указаны лимиты `5 запросов в сек и 100_000 в день`, но у меня чаще чем в 20 сек вылетала ошибка `Max rate limit reached, please use API Key for higher rate limit`. Лучше ротейтить ключи, но вылетала ошибка, что апи ключи просто не используется `Invalid API Key`.

### UPD:

Оказывается он начинает работает только через некоторое время.

# Описание

## Запуск

1. Создать и заполнить `.env` файл соответсвующими значениями (для примера есть файл `.env.example`), где:

- `PG_USERNAME` - имя PostgreSQL пользователя
- `PG_PASSWORD` - пароль PostgreSQL пользователя
- `PG_DB_NAME` - название бд PostgreSQL
- `PG_HOST` - адрес, где запущен PostgreSQL
- `PG_PORT` - порт, на котором запущен PostgreSQL

2. Установить зависимости

```bash
$ npm i
```

3. Запустить:

### для разработка

```bash
$ npm run dev
```

### для продакшена

```bash
$ npm run build
$ npm run start
```

## Тестирование

Тесты не реализованы (дольше и не уверен что кто-то их смотрел бы :) )
