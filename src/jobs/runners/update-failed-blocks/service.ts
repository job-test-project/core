import { Injectable, Logger } from '@nestjs/common';
import { Cron } from '@nestjs/schedule';

import { blocksRepository } from '../../../models';
import { BlocksService } from '../../abstract/blocks-service';

@Injectable()
export class UpdateFailedBlocksService extends BlocksService {
  public readonly logger = new Logger(UpdateFailedBlocksService.name);
  private isRunning = false;

  private async updateFailedBlocks(): Promise<void> {
    const latestFailedBlock = await blocksRepository.findLatestFailedBlock();

    if (!latestFailedBlock) {
      return;
    }

    this.logger.debug(`Fixing block number ${latestFailedBlock.id}`);

    await this.processBlockNumber(Number(latestFailedBlock.id), false);
  }

  @Cron('*/20 * * * * *')
  async handleCron(): Promise<void> {
    if (this.isRunning) {
      this.logger.warn(`Job is already running, aborting...`);
      return;
    }
    this.isRunning = true;

    this.logger.log('Job started');

    try {
      const startTime = Date.now();

      await this.updateFailedBlocks();

      this.logger.log(`Job finished in ${Date.now() - startTime}ms`);
    } catch (e) {
      this.logger.error(`Job failed - ${e.message}`);
    }

    this.isRunning = false;
  }
}
