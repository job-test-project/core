export const etherscanConfig = {
  baseURL: 'https://api.etherscan.io/api',
  timeoutPerRequest: 10000,
} as const;
