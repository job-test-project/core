import { Transaction } from 'sequelize';

import { ETxStatuses, TxsEntity } from '@project/project-models';

export class TxsRepository {
  async create(
    tx: {
      hash: string;
      blockId: string;
      from: string;
      to: string;
      value: string;
    },
    opts?: { t?: Transaction },
  ): Promise<TxsEntity> {
    return await TxsEntity.create(
      {
        hash: tx.hash,
        block_id: tx.blockId,
        from: tx.from,
        to: tx.to,
        value: tx.value,
        status: ETxStatuses.UNKNOWN, // etherscan returns no info about tx status
      },
      { transaction: opts?.t },
    );
  }
}
