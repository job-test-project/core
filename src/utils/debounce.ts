export function getDebouncer(ms: number) {
  let availableAfter = 0;

  return function func<T>(callback: () => Promise<T>): Promise<T> {
    return new Promise((res, rej) => {
      const now = Date.now();
      let timeout = 0;

      if (availableAfter > now) {
        timeout = availableAfter - now;
      }

      setTimeout(async () => {
        try {
          const result = await callback();
          res(result);
        } catch (e) {
          rej(e);
        }
      }, timeout);

      availableAfter = Date.now() + timeout + ms;
    });
  };
}
